#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>

/* define all scheme constants */
#define bool_f             0x2F
#define bool_t             0x6F
#define fx_mask            0x03
#define fx_tag             0x00
#define fx_shift              2
#define empty_tag          0x3F
#define char_tag           0x0F
#define char_mask          0xFF
#define char_shift            8

/* all scheme values are of type ptrs */
typedef unsigned int ptr;

static char* allocate_protected_space(int size){
  int page = getpagesize();
  int status;
  int aligned_size = ((size + page - 1) / page) * page;
  char* p = mmap(0, aligned_size + 2 * page,
		 PROT_READ | PROT_WRITE,
		 MAP_ANONYMOUS | MAP_PRIVATE,
		 0, 0);
  if (p == MAP_FAILED) {
    fprintf(stderr, "Runtime: mmap failed\n");
    exit(EXIT_FAILURE);
  }
  status = mprotect(p, page, PROT_NONE);
  if(status != 0){
    fprintf(stderr, "Runtime: mprotect failed\n");
    exit(EXIT_FAILURE);
  }
  status = mprotect(p + page + aligned_size, page, PROT_NONE);
  if(status != 0){
    fprintf(stderr, "Runtime: mprotect failed\n");
    exit(EXIT_FAILURE);
  }
  return (p + page);
}

static void deallocate_protected_space(char* p, int size){
  int page = getpagesize();
  int status;
  int aligned_size = ((size + page - 1) / page) * page;
  status = munmap(p - page, aligned_size + 2 * page);
  if (status != 0)   {
    fprintf(stderr, "Runtime: munmap failed\n");
    exit(EXIT_FAILURE);
  }
}

    
    
static void print_ptr(ptr x){
  if((x & fx_mask) == fx_tag){
    printf("%d", ((int)x) >> fx_shift);
  } else if(x == bool_f){
    printf("#f");
  } else if(x == bool_t){
    printf("#t");
  } else if(x == empty_tag){
    printf("()");
  } else if((x & char_mask) == char_tag){
    char c = ((int)x) >> char_shift;
    if (c=='\n') {
      printf("#\\newline");
    } else if (c==' '){
      printf("#\\space");
    } else if (c=='\r'){
      printf("#\\return");
    } else if (c=='\t'){
      printf("#\\tab");
    } else {
      printf("#\\%c", ((int)x) >> char_shift);
    }
  } else{
    printf("#<unknown 0x%08x>", x);
  }
  printf("\n");
}

int main(int argc, char** argv){
  int stack_size = (16 * 4096); /* holds 16K cells */
  char* stack_top = allocate_protected_space(stack_size);
  char* stack_base = stack_top + stack_size;
  print_ptr(scheme_entry(stack_base));
  deallocate_protected_space(stack_top, stack_size);
  return 0;
}
      
